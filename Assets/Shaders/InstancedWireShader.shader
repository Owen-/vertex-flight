﻿// Upgrade NOTE: replaced 'UNITY_INSTANCE_ID' with 'UNITY_VERTEX_INPUT_INSTANCE_ID'

Shader "Vertex Flight/InstancedWireShader"
{
    Properties
    {
    	_Tex ("Damage Texture", 2D) = "white" {}
    	_Mask ("Masking Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _MaskCutoff ("Mask Cutoff", float) = 1
        _Freq ("Pulse Frequency", float) = 1
        _DamageCutoff ("Damage Texture Relevant Threshold", float) = 1
        [HDR] _EmissionColor ("Emission Color", Color) = (0,0,0)
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            UNITY_INSTANCING_CBUFFER_START (MyProperties)
            UNITY_DEFINE_INSTANCED_PROP (float4, _Color)
            UNITY_DEFINE_INSTANCED_PROP (float, _MaskCutoff)
            UNITY_INSTANCING_CBUFFER_END

            sampler2D _Tex;
            sampler2D _Mask;

            float4 _Tex_ST;
            float4 _Mask_ST;

            float _DamageCutoff;
            float _Freq;
           
            v2f vert (appdata v)
            {
                v2f o;

                UNITY_SETUP_INSTANCE_ID (v);
                UNITY_TRANSFER_INSTANCE_ID (v, o);

                o.vertex = UnityObjectToClipPos (v.vertex);

                o.uv2 = TRANSFORM_TEX(v.uv, _Mask);

                o.uv = TRANSFORM_TEX(v.uv, _Tex);

                return o;
            }
           
            fixed4 frag (v2f i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID (i);
            	fixed4 damagedCol = 0; // undamaged - no damage texture comes through
            	fixed4 maskCol = tex2D(_Mask, i.uv2);
                fixed4 emissionCol = tex2D(_Tex, i.uv);

            	float time = sin(_Time[1] * _Freq); //in the vert part please

                if (time< 0.2) time = 0.2;

            	//0 mask does nothing
            	if (maskCol.x >= UNITY_ACCESS_INSTANCED_PROP (_MaskCutoff)) //lower mask cutoff = more damage texture visible
            	{
	        		damagedCol = 1-tex2D(_Tex, i.uv);
	        		if (damagedCol.x < _DamageCutoff) //lower damage cutoff cutoff = more of this
	        		{
	        			damagedCol = 0;
	        		}
            	}


            	fixed4 instanceCol = UNITY_ACCESS_INSTANCED_PROP (_Color);
                
            	fixed4 finalCol = instanceCol + damagedCol*time;
                return finalCol;
            }
            ENDCG
        }
    }
}
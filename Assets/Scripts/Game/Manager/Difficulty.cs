using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Animation Curves to set up things?

[System.Serializable]
[CreateAssetMenu(fileName = "Difficulty", menuName = "Vertex Flight/Difficulty", order = 1)]
public class Difficulty : ScriptableObject 
{
	public WeightedEnemySettings[] weightedEnemySettings;

	public float minSpawnDelay = 0.5f;
	public float maxSpawnDelay = 3f;
	public bool isBoss = false;

	public float enemySpawnDelay
	{
		get 
		{
			return Random.Range(minSpawnDelay, maxSpawnDelay);
		}
	} 

	[System.Serializable]
	public class WeightedEnemySettings
	{
		public EnemySettings settings;
		public int weight;
	}

	public EnemySettings GetEnemySettings()
	{
		int total = 0;
		for (int i = 0; i< weightedEnemySettings.Length; i++)
		{
			total += weightedEnemySettings[i].weight;
		}

		int rnd = Random.Range(0,total+1);
		total = 0;
		for (int i = 0; i< weightedEnemySettings.Length; i++)
		{
			total += weightedEnemySettings[i].weight;
			if (total >= rnd)
				return weightedEnemySettings[i].settings;
		}
		return null;
	}

}

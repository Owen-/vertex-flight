﻿using UnityEngine;
using System.Collections;

public struct SavedVariables 
{
	private const string credits 				= "credits";
	private const string levelHighscoreFormat 	= "{0}_highscore";
	private const string levelUnLockedFormat 	= "{0}_isUnlocked";
	private const string pickupFormat 			= "{0}_unlockedCount";

	public static bool IsLocked(LevelSettings level)
	{
		string prefsName = string.Format(levelUnLockedFormat, level.GetName());
		return PlayerPrefs.GetInt(prefsName) == 0;
	}

	public static void Unlock(LevelSettings level)
	{
		string prefsName = string.Format(levelUnLockedFormat, level.GetName());
		PlayerPrefs.SetInt(prefsName, 1);
	}

	public static void Lock(LevelSettings level)
	{
		string prefsName = string.Format(levelUnLockedFormat, level.GetName());
		PlayerPrefs.SetInt(prefsName, 0);
	}

	public static int GetHighScore(LevelSettings level)
	{
		string prefsName = string.Format(levelHighscoreFormat, level.GetName());
		return PlayerPrefs.GetInt(prefsName);
	}

	public static void SetHighScore(LevelSettings level, int score)
	{
		string prefsName = string.Format(levelHighscoreFormat, level.GetName());
		PlayerPrefs.SetInt(prefsName, score);
	}

	public static void SetItemUnlockedCount(ItemType type, int i)
	{
		string prefsName = string.Format(pickupFormat, type.ToString());
		PlayerPrefs.SetInt(prefsName, i);
	}

	public static int GetItemUnlockedCount(ItemType type)
	{
		string prefsName = string.Format(pickupFormat, type.ToString());
		return PlayerPrefs.GetInt(prefsName);
	}

	public static int GetCredits()
	{
		return PlayerPrefs.GetInt(credits);
	}

	public static void SetCredits(int i)
	{
		PlayerPrefs.SetInt(credits, i);
	}
}
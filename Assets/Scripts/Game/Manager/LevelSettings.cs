﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Vertex Flight/LevelSettings", order = 1)]
public class LevelSettings : ScriptableObject 
{
	[Header("Level Config")]
	public float playerStartDelay = 1f;
	public float spawnZoneBuffer = 1f;
	public int initialPlayerLives = 3;
	public float hyperFlightDuration = 10f;
	public float hyperFlightBonus = 2f;
	public bool isLockable = true;
	public bool isHyper = false;
	[SerializeField] private string levelName;

	[Header("Difficulty")]
	public DifficultyWithTime[] difficulties;

	[System.Serializable]
	public class DifficultyWithTime
	{
		public Difficulty difficulty;
		public float time;
	}

	public GUIStyle guiStyle
	{
		get
		{
			GUIStyle style = new GUIStyle(GUI.skin.button);
			style.fontSize = 40;
			return style;
		}
	}

	//should be sorted
	public Difficulty GetDifficulty(float t)
	{
		if (difficulties.Length <= 0)
		{
			Debug.LogError("No Difficulties Set Up");
			return null;
		}

		for (int i=0; i< difficulties.Length; i++)
		{
			if (difficulties[i].time >= t)
			{
				return difficulties[i].difficulty;
			}
		}

		return difficulties[difficulties.Length-1].difficulty;
	}

	public string GetInfo()
	{
		if ( IsLocked() )
			return "Locked";
		else
			return "Highscore: " + GetHighScore();

	}

	public string GetName()
	{
		return levelName;
	}

	public int GetHighScore()
	{
		return SavedVariables.GetHighScore(this);
	}

	public void SetHighScore(int score)
	{
		SavedVariables.SetHighScore(this, score);
	}

	public bool IsLocked()
	{
		if (isLockable)
			return SavedVariables.IsLocked(this);
		return false;
	}

	public void Unlock()
	{
		SavedVariables.Unlock(this);
	}

	public void Lock()
	{
		if (isLockable)
			SavedVariables.Lock(this);
	}
}

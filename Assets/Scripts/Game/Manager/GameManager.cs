﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public static GameManager instance = null;
	public static LevelSettings CurrentLevel
	{
		get { return currentLevel; }
	}
	private static LevelSettings currentLevel;

	public delegate void GameScore(int i);
	public delegate void GameEvent();
	public event GameScore OnScore;
	public event GameEvent OnStartGame, OnEndGame;

	public AnimationCurve hpfCurve;

	[Header("Prefabs")]
	public EnemyPool enemyPool;
	public TextPool rewardPointsTextPool;
	public PickupPool pickupPool;
	public Player playerPrefab;

	[Header("Levels")]
	public LevelSettings[] levels;


	private Player instantiatedPlayer;
	private int playerLives;
	private float hyperFlightDuration;
	private bool gameIsPlaying = false;
	private AudioHighPassFilter highPassFilter;
	private float difficultyTimer = 0f; 
	private bool difficultyTimerPaused = false;
	private bool lerpHPFOff = false;
	private bool bossJustDied = false;
	private int highscore, score;

	private void Awake()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(gameObject);

		currentLevel = levels[0];

		highPassFilter = GetComponent<AudioHighPassFilter>();
		enemyPool.Initialize();
		rewardPointsTextPool.Initialize();
		pickupPool.Initialize();

		OnEndGame+=ClearGameplayElements;
		OnEndGame+=SetupMenuState;
		OnEndGame+=TrySaveHighScore;

		SetupMenuState();

		Invoke("SpawnEnemy", 0f);
	}

	public bool HasBossJustDied()
	{
		return bossJustDied;
	}

	private void SetupMenuState()
	{
		if (highPassFilter != null)
			highPassFilter.cutoffFrequency = 5000;

		gameIsPlaying = false;
		difficultyTimerPaused = true;
		difficultyTimer=0;
	}

	public bool IsPlaying()
	{
		return gameIsPlaying;
	}

	public void ChangeLevel(bool forwards)
	{
		for (int i = 0; i< levels.Length; i++)
		{
			if (levels[i] == currentLevel)
			{
				int next = forwards? i+1 : i-1;
				if (next == -1) next = levels.Length-1;
				else if (next == levels.Length) next = 0;

				currentLevel = levels[next];
				return;
			}
		}
		Debug.LogError("Current level is not in level list!");
	}

	public Player GetPlayer() // maybe static?
	{
		return instantiatedPlayer;
	}

	public void StartGame()
	{
		ClearGameplayElements();
		playerLives = currentLevel.initialPlayerLives + SavedVariables.GetItemUnlockedCount(ItemType.Lives);
		hyperFlightDuration = currentLevel.hyperFlightDuration + SavedVariables.GetItemUnlockedCount(ItemType.HyperFlight)*currentLevel.hyperFlightBonus;

		difficultyTimer = 0;
		score = 0;
		highscore = currentLevel.GetHighScore();
		difficultyTimerPaused = false;
		lerpHPFOff=true;
		gameIsPlaying = true;
		bossJustDied = false;
		
		Invoke("SpawnEnemy", 0.5f);
		Invoke("SpawnPlayer", currentLevel.playerStartDelay);

		OnStartGame();
	}


	public void RewardPoints(int points, Vector2 position)
	{
		RewardPointsText RPT = rewardPointsTextPool.TryProduce(position);
		if (RPT != null) RPT.Initialize(points);

		ScorePoints(points);
	}

	public void ScorePoints(int points) // perhaps this should be IScoreable again
	{
		if(gameIsPlaying)
		{
			score += points;
			OnScore( points );
		}
	}

	public void PlayerDeath()
	{
		if(instantiatedPlayer.GetLives() <= 0) 
			EndGame();
		else
		{
			playerLives = instantiatedPlayer.GetLives();
			Invoke("SpawnPlayer", currentLevel.playerStartDelay);
		}
	}

	private void ClearGameplayElements()
	{
		CancelInvoke();
		enemyPool.HideAll();
		pickupPool.HideAll();
		if(instantiatedPlayer!=null)
			instantiatedPlayer.Die();
	}

	public int GetScore()
	{
		return score;
	}

	public void UnlockNextLevel()
	{
		bossJustDied=true;
		EndGame();
		ClearGameplayElements();
		for (int i = 0; i< levels.Length; i++)
		{
			if (levels[i] == currentLevel)
			{
				int next = i+1;
				if (next == levels.Length) return; // maybe give achiev or notice at least

				levels[next].Unlock();
				return;
			}
		}
	}

	public void SpawnPickup(Vector2 position)
	{
		Pickup P = pickupPool.TryProduce(position);
		if (P!=null) 
			P.Initialize();
	}

	private void SpawnEnemy()
	{
		Difficulty D = currentLevel.GetDifficulty(difficultyTimer);

		Enemy E = enemyPool.TryProduce(RandomLocationAboveTopOfScreen());
		E.Initialize(D.GetEnemySettings());
		
		if (!D.isBoss)
			Invoke("SpawnEnemy", D.enemySpawnDelay);
		else
			SoundManager.instance.PlayBossSpawn();
	}

	private void EndGame()
	{
		SavedVariables.SetCredits(SavedVariables.GetCredits() + score);
		OnEndGame();
	}

	public void BossPassed()
	{
		if(gameIsPlaying)
		{
			EndGame();
			ClearGameplayElements();
			
		}
	}

	private void Update()
	{
		if (!difficultyTimerPaused) difficultyTimer += Time.deltaTime;
		if (lerpHPFOff && difficultyTimer < 1f)
			highPassFilter.cutoffFrequency = 5000*hpfCurve.Evaluate(difficultyTimer*2f);
	}

	private void SpawnPlayer()
	{
		if (playerPrefab == null)
			Debug.LogError("No Player Prefab Assigned to GameManager");

		difficultyTimerPaused=false;

		if(instantiatedPlayer==null)
			instantiatedPlayer = Instantiate(playerPrefab, transform.position, Quaternion.identity) as Player;
		
		instantiatedPlayer.Initialize(playerLives, hyperFlightDuration);
	}

	private Vector2 RandomLocationAboveTopOfScreen()
	{
		Vector2 loc = Camera.main.ViewportToWorldPoint(new Vector2(Random.value,1f)); 
		loc.y += currentLevel.spawnZoneBuffer;


		return loc;
	}

	private void TrySaveHighScore()
	{
		if (highscore < score)
		{
			highscore = score;
			currentLevel.SetHighScore(highscore);
		}
	}

	private void OnDestroy()
	{
		OnEndGame-=TrySaveHighScore;
		OnEndGame-=SetupMenuState;
		OnEndGame-=ClearGameplayElements;
	}

	public void OnHome()
	{
		Invoke("SpawnEnemy", 0f);
	}
}

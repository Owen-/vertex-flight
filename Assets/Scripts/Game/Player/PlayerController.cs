﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour 
{
	public AnimationCurve lerpCurve;

	public void Initialize()
	{
		transform.position = GetDesiredPosition();
	}

	private void FixedUpdate()
	{
		Move();
	}

	private void Move()
	{
		Vector2 newPos = GetDesiredPosition();

		float difference = Vector2.Distance(transform.position, newPos);

		newPos = Vector2.Lerp (transform.position, newPos, lerpCurve.Evaluate(difference)); // perhaps max speed should be clamped, but animation curve seems to do a great job
		
		transform.position = newPos;
	}

	private Vector2 GetDesiredPosition()
	{
		Vector2 input = transform.position;
		#if UNITY_IPHONE || UNITY_ANDROID
		if(Input.touchCount > 0)
		{
			Touch T = Input.GetTouch(0);
			input = T.position;
		}
		#endif
		#if UNITY_EDITOR
			input = Input.mousePosition;
		#endif


		if (!CheckTouchedPlayableArea(input)) 
			return transform.position;
		
		return Camera.main.ScreenToWorldPoint(input);
	}


	private bool CheckTouchedPlayableArea(Vector2 input)
	{
		return Camera.main.pixelRect.Contains(input);
	}

}

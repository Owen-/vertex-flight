﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour 
{
	public float fireDelay = 1f;
	private float initialFireDelay = 1f;
	public PlayerController playerController;

	public List<Transform> barrels;
	public Transform[] potentialBarrels;

	public TextMesh livesText;

	public BulletPool ammoPool; 

	private bool isHyperFlight = false;
	private float hyperflightDuration = 10f;

	private int lives;
		
	private void Awake()
	{
		ammoPool.Initialize();
		initialFireDelay = fireDelay;
	}

	public void Initialize(int lives, float hyperflightDuration)
	{
		this.lives = lives;
		UpdateLivesText();
		gameObject.SetActive(true);

		fireDelay = initialFireDelay;

		barrels = new List<Transform>()
		{
			barrels[0], barrels[1]
		};
		
		for (int i=0; i<SavedVariables.GetItemUnlockedCount(ItemType.Rate); i++)
			IncreaseRoF();

		for (int i=0; i<SavedVariables.GetItemUnlockedCount(ItemType.Upgrade); i++)
			Upgrade();

		InvokeRepeating("Fire", 0f, fireDelay);
		playerController.Initialize();
	}

	public void HyperFlight()
	{
		isHyperFlight=true;
		Invoke("HyperFlightOff", hyperflightDuration);
		Time.timeScale = 1.5f;
	}

	private void HyperFlightOff()
	{
		isHyperFlight=false;
		Time.timeScale = 1.0f;
	}

	public void GainLife()
	{
		lives++;
		UpdateLivesText();
	}

	private void UpdateLivesText()
	{
		if (livesText !=null) 
			livesText.text = lives.ToString();
		
	}

	public void Upgrade()
	{
		if (potentialBarrels.Length < barrels.Count) return;

		Transform B1 = potentialBarrels[barrels.Count];
		Transform B2 = potentialBarrels[barrels.Count+1];

		barrels.Add(B1);
		barrels.Add(B2);
	}

	public void IncreaseRoF()
	{
		CancelInvoke("Fire");
		fireDelay *= 0.8f;
		if (fireDelay < 0.15f) fireDelay = 0.15f;
		InvokeRepeating("Fire", 0f, fireDelay);
	}
	
	private void Fire()
	{
		foreach (Transform barrel in barrels)
		{
			Bullet B = ammoPool.TryProduce(barrel.position);
			if (B != null)
			{
				B.Initialize();
				SoundManager.instance.PlayShoot(0);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.GetComponent<Enemy>() != null && !isHyperFlight)
		{
			lives--;
			GameManager.instance.PlayerDeath();
			Die();
			return;
		}

		Pickup pickup = other.gameObject.GetComponent<Pickup>();
		
		if (pickup != null)
		{
			SoundManager.instance.PlayPickup();

			if (pickup.upgrade) Upgrade();
			if (pickup.rate) IncreaseRoF();
			if (pickup.life) GainLife();
			if (pickup.hyperFlight && !isHyperFlight) HyperFlight();

			GameManager.instance.RewardPoints(pickup.points, pickup.transform.position);
			pickup.Die();
		}

	}

	public int GetLives()
	{
		return lives;
	}

	public void Die()
	{
		CancelInvoke();
		ammoPool.HideAll();
		gameObject.SetActive(false);
	}

}

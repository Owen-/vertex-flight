﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//should not collide with things offscreen
public class Bullet : PooledObject 
{
	public float speed = 1f;
	public int damage = 1;
	public int power = 1;

	//public float initialSpeed = 1f;
	public int initialDamage = 1;
	public int initialPower = 1;

	private int bounceCount = 0;
	private int maxBounceCount;
	private Vector3 direction;

	private float killRadius;

	private void Awake()
	{
		killRadius = Camera.main.ViewportToWorldPoint(new Vector2(0.75f,0.75f)).magnitude;
	}

	public void Initialize()
	{
		direction = transform.up;
		maxBounceCount = SavedVariables.GetItemUnlockedCount(ItemType.Bouncing);
		damage = initialDamage + SavedVariables.GetItemUnlockedCount(ItemType.Damage);
		power = initialPower + SavedVariables.GetItemUnlockedCount(ItemType.Power);
	}
	
	private void FixedUpdate () 
	{
		transform.position += direction * speed * Time.deltaTime;
		CheckLeftScreen();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		IDamageable ID = other.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
		if (ID != null)
		{
			ID.DoDamage(damage, power);
			TryDie();
		}
	}

	private void CheckLeftScreen()
	{
		if(transform.position.magnitude >= killRadius)
			Die();
	}

	private void TryDie()
	{
		if (maxBounceCount > bounceCount)
		{
			transform.Rotate(new Vector3(0f,0f,Random.value*360f));
			direction = transform.up;
			bounceCount++;
		}
		else Die();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "EnemySettings", menuName = "Vertex Flight/EnemySettings", order = 1)]
public class EnemySettings : ScriptableObject
{
	public int health = 1;
	public float scale = 1f;
	public Color color = Color.red;
	public int killValue = 10;

	public float mass = 1f;
	public float speed = 1f;

	public bool isBoss = false;
	public MovementAI movementType;

	[Range(0f,1f)] public float lootProbability;
}

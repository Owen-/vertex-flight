using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour
{
	private float speed;
	private float mass;
	private Vector3 direction;
	private float killRadius;
	private MovementAI movementType;
	private float offset;

	public void Initialize(float mass, float speed, MovementAI type)
	{
		killRadius = transform.position.magnitude*1.5f;
		direction = Vector2.up * -1f;
		this.speed = speed;
		this.mass = mass;
		movementType = type;
		offset = Time.time;
	}

	private void FixedUpdate()
	{
		Move();
	}

	private void Move()
	{
		transform.position += (Vector3)direction * speed * Time.deltaTime;
		if(movementType == MovementAI.Homing)
		{
			Player P = GameManager.instance.GetPlayer();
			if (P!=null)
				transform.position +=(P.transform.position-transform.position).normalized*Time.deltaTime*speed/2f;
		}
		else if (movementType == MovementAI.Sine)
		{
			transform.position +=Vector3.right*Mathf.Sin((Time.time-offset)*speed)*Time.deltaTime *mass;
		}
	}

	public void PushBack(int damage)
	{
		Vector3 pushBackVector = Vector3.up * (damage/5f) / mass;
		if (pushBackVector.magnitude > 1.5f)
			pushBackVector = pushBackVector.normalized*1.5f;
		transform.position +=  pushBackVector;
	}
	public bool HasLeftScreen() // not sure how costly this is but seems nicer
	{
		return transform.position.magnitude >= killRadius;
	}
}

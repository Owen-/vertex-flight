﻿using UnityEngine;
using System.Collections;

public class Pickup : PooledObject 
{
	public TextMesh pickupText;

	public bool upgrade;
	public bool rate;
	public bool life;
	public bool hyperFlight;
	public int points;

	public void Initialize()
	{
		rate = (Random.value > 0.15f)? false : true;
		if (rate) 
		{
			pickupText.text = "Rate";
			return;
		}

		
		life = (Random.value > 0.10f)? false : true;
		if (life) 
		{
			pickupText.text = "+ 1";
			return;
		}
		
		hyperFlight = (Random.value > 0.01f)? false : true;
		if(hyperFlight) 
		{
			pickupText.text = "Hyper";
			return;
		}

		upgrade = (Random.value > 0.01f)? false : true;
		if (upgrade) 
		{
			pickupText.text = "Guns";
			return;
		}

		points = Random.Range(50, 500);
		pickupText.text = points.ToString();
	}
}

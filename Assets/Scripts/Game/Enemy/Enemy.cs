using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : PooledObject, IKillable, IDamageable
{
	public Renderer rend;
	public EnemyController controller;

	public EnemySettings settings; // use only for Initialization reference

	public int health;
	private MaterialPropertyBlock shaderProperties;

	private bool isHyper = false;

	public void Initialize(EnemySettings settings) 
	{
		this.settings = settings;
		isHyper = GameManager.CurrentLevel.isHyper;
		
		if (isHyper)
		{
			float r = Random.Range(0.25f,1.25f);
			gameObject.transform.localScale = new Vector3 (r, r, r);
			shaderProperties.SetColor("_Color", Random.ColorHSV());
		}
		else
		{
			gameObject.transform.localScale = new Vector3 (settings.scale, settings.scale, settings.scale);
			shaderProperties.SetColor("_Color", settings.color);
		}
		shaderProperties.SetFloat("_MaskCutoff", 1);
		rend.SetPropertyBlock(shaderProperties);
		health = settings.health;
		
		controller.Initialize(settings.mass, settings.speed, settings.movementType);

	}

	private void Awake()
	{
		shaderProperties = new MaterialPropertyBlock();
	}

	private void Update()
	{
		if (controller.HasLeftScreen())
		{
			DieVisual();
		}
	}


	public void DoDamage(int damage, int power)
	{
		health -= damage;
		if (health <= 0)
		{
			DieFromPlayer();
		}
		else
		{
			shaderProperties.SetFloat("_MaskCutoff", (float)(health-1)/settings.health);
			rend.SetPropertyBlock(shaderProperties);
			controller.PushBack(power);
			SoundManager.instance.PlayDamage();
		}
	}

	public void DieVisual()
	{
		//explode or something
		Die();
		if(settings.isBoss)
		{
			GameManager.instance.BossPassed();
		}
	}

	public void DieFromPlayer()
	{
		SoundManager.instance.PlayExplosion();
		GameManager.instance.RewardPoints(GetScorePoints(), transform.position); 

		if(settings.isBoss)
			GameManager.instance.UnlockNextLevel();

		if (Random.value < settings.lootProbability)
			GameManager.instance.SpawnPickup(transform.position);

		DieVisual();
	}

	public int GetScorePoints()
	{
		return settings.killValue;
	}
}

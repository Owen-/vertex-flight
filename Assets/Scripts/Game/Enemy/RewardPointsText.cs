﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RewardPointsText : PooledObject 
{
	public TextMesh T;
	public Vector2 floatVector;

	public void Initialize(int i, float t = 0.5f)
	{
		lifetime = t;
		T.text = i.ToString();
	}

	private void FixedUpdate()
	{
		transform.position += (Vector3)floatVector * Time.deltaTime;
	}
}

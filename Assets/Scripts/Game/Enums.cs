﻿using System.Collections;

public enum ItemType 
{ 
	HyperFlight, 
	Bouncing, 
	Homing,
	Shields,
	Magnet,
	Lives,
	Damage,
	Power,
	Upgrade,
	Rate
}

public enum PickupType
{
	Points,
	HyperFlight,
	RateOfFire,
	Upgrade,
	Life
}

public enum MovementAI
{
	Linear,
	Sine,
	Homing,
}


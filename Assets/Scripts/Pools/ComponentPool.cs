﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Instantiating a component is the same as instantiating its GameObject, but returns a component, not a GameObject 
// so we avoid costly GetComponent<>() for initialization, assuming you only need to initialize one thing. 

[System.Serializable]
public class ComponentPool<T> : ObjectPool<T> where T : Component
{
	public T prefabComponent;
	
	private Transform containerTransform;

	public override void Initialize()
	{
		containerTransform = new GameObject("Pooled " + typeof(T).Name).transform;
		base.Initialize();
	}

	protected override T MakeObject()
	{
		T t = (T) MonoBehaviour.Instantiate(prefabComponent);
		
		t.gameObject.SetActive(false);
		t.transform.parent=containerTransform;
		
		return t;
	}

	protected override bool IsActive(T pooledComponent)
	{
		return pooledComponent.gameObject.activeInHierarchy;
	}

	public T TryProduce(Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
	{
		T t = GetObject();
		if (t != null)
		{
			t.transform.position = position;
			t.transform.rotation = rotation;
			t.gameObject.SetActive(true);
		}
		return t;
	}

	public void HideAll()
	{
		foreach (T t in pooledObjects)
		{
			if (IsActive(t))
				t.gameObject.SetActive(false);
		}
	}
}

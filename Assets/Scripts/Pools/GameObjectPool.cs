﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GameObjectPool : ObjectPool<GameObject>
{
	public GameObject prefab;
	
	private Transform containerTransform;
	private List<GameObject> pooledGameObjects;

	public override void Initialize()
	{
		containerTransform = new GameObject("Pooled Objects").transform;
		base.Initialize();
	}


	protected override GameObject MakeObject()
	{
		GameObject obj = (GameObject) MonoBehaviour.Instantiate(prefab);
		
		obj.SetActive(false);
		obj.transform.parent=containerTransform;
		 
		return obj;
	}

	protected override bool IsActive(GameObject pooledObject)
	{
		return pooledObject.activeInHierarchy;
	}

	public GameObject TryProduce(Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion))
	{
		GameObject GO = GetObject();
		if (GO != null)
		{
			GO.transform.position = position;
			GO.transform.rotation = rotation;
			GO.SetActive(true);
		}
		return GO;
	}
}

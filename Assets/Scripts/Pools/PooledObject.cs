﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledObject : MonoBehaviour 
{
	public float lifetime = 10f;
	private void OnEnable()
	{
		Invoke("Die", lifetime);
	}

	private void OnDisable()
	{
		CancelInvoke();
	}

	public void Die()
	{
		gameObject.SetActive(false);
	}
}

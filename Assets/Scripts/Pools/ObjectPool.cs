﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class ObjectPool<T> where T : Object
{
	public int maxAmount = 10;
	public int initialAmount = 5;
	public bool canGrow = true;
	
	protected List<T> pooledObjects;

	public virtual void Initialize()
	{
		pooledObjects = new List<T>();

		for (int i = 0; i < initialAmount; i++)
		{
			pooledObjects.Add( MakeObject() );
		}
	}

	public T GetObject()
	{
		for (int i =0; i< pooledObjects.Count; i++)
		{
			if (!IsActive(pooledObjects[i]))
			{
				return pooledObjects[i];
			}
		}

		if (canGrow && pooledObjects.Count < maxAmount )
		{
			return MakeObject();
		}

		return null;
	}

	protected abstract T MakeObject();
	protected abstract bool IsActive(T pooledObject);
	//public abstract T TryProduce();
}

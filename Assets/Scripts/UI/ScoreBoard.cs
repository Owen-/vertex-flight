﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.UI;
using System.Collections;

public class ScoreBoard : MonoBehaviour 
{
	private int score = 0;
	private int highscore;
	private bool flashScore = false;
	public Color initialColor;
	public Text scoreText;

	#if UNITY_EDITOR
	[MenuItem("CONTEXT/ScoreBoard/Reset Highscores")]
	public static void ResetHighScores()
	{
		LevelSettings[] levels = GameManager.instance.levels;
		foreach (LevelSettings level in levels)
		{
			level.Lock();
			level.SetHighScore(0);
		}
	}
	#endif

	private void Start()
	{
		Initialize();

		GameManager.instance.OnStartGame += Initialize;
		GameManager.instance.OnScore += ModifyScore;
	}

	private void Initialize()
	{
		if (scoreText==null) 
			Debug.LogError("No Score Text Assigned");

		scoreText.color = initialColor;

		highscore = GameManager.CurrentLevel.GetHighScore();
		score = GameManager.instance.GetScore();

		flashScore = false;

		scoreText.text = score.ToString("D8");
	}

	private void Update()
	{
		if (flashScore) FlashScore();
	}

	private void ModifyScore(int i)
	{
		score = GameManager.instance.GetScore();

		scoreText.text = score.ToString("D8");

		if (highscore < score && flashScore==false && highscore > 0)
		{
			SoundManager.instance.PlayBeatHighScore();
			flashScore=true;
		}
	}

	private void FlashScore()
	{
		scoreText.color = Random.ColorHSV();
	}

	private void OnDestroy()
	{
		GameManager.instance.OnScore -= ModifyScore;
		GameManager.instance.OnStartGame -= Initialize;
	}
}

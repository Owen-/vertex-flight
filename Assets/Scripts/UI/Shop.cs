﻿using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Shop : MonoBehaviour 
{
	public Text creditsText;
	public ShopItem[] items;

	//private List<ShopItem> instantiatedShopItems;

	public RectTransform container;

	#if UNITY_EDITOR
	[MenuItem("CONTEXT/Shop/Reset Shop Items")]
	public static void ResetShopItems()
	{
		foreach (ItemType type in Enum.GetValues(typeof(ItemType)).Cast<ItemType>())
		{
			SavedVariables.SetItemUnlockedCount(type, 0);
		}
		SavedVariables.SetCredits(0);
	}
	#endif

	private void Awake()
	{
		//instantiatedShopItems = new List<ShopItem>();
		foreach (ShopItem I in items)
		{
			//ShopItem temp = Instantiate (I);
			//temp.transform.SetParent(container);
			I.Initialize(this); // add a listener instead of this?
			//instantiatedShopItems.Add(temp);
		}
		UpdateUI();
	}

	public void ShowShop()
	{
		UpdateUI();
		foreach (ShopItem I in items)
		{
			I.UpdateUI();
		}
	}

	public void UpdateUI()
	{
		creditsText.text = "Credits: " + SavedVariables.GetCredits();
	}
}

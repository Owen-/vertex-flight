﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// would like this to ensure the texture is always square too
public class BackgroundScroller : TextureScroller 
{
	public float tiling = 10f;
	new private void Start()
	{
		base.Start();
		Camera cam = Camera.main;
		Vector3 scale = new Vector3(cam.orthographicSize*2f * (9.0f / 16.0f),cam.orthographicSize*2f,1f);
		transform.localScale = scale;
	}
}

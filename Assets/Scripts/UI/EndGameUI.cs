﻿using UnityEngine;
using UnityEngine.UI;

public class EndGameUI : MonoBehaviour 
{
	private Canvas canvas;
	public Text resultScoresText;
	public Text gameOverText;
	private void Start()
	{
		GameManager.instance.OnEndGame += ShowCanvas;
		GameManager.instance.OnStartGame += HideCanvas;
		canvas = GetComponent<Canvas>();
	}

	private void OnDestroy()
	{
		GameManager.instance.OnEndGame -= ShowCanvas;
		GameManager.instance.OnStartGame -= HideCanvas;
	}

	public void ShowCanvas()
	{
		if (canvas!=null) canvas.enabled = true;

		LevelSettings L = GameManager.CurrentLevel;
		resultScoresText.text = "Credits Earned: " + GameManager.instance.GetScore() + "\nHighscore: " + L.GetHighScore();
		gameOverText.text = GameManager.instance.HasBossJustDied() ? "You Won!" : "Game Over";
	}

	public void HideCanvas()
	{
		if (canvas!=null) canvas.enabled = false;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class SoundToggle : MonoBehaviour 
{
	public Text soundText;

	public void ToggleSound(bool b)
	{
		if (!b)
			AudioListener.volume = 0f;
		else
			AudioListener.volume = 1f;

		soundText.text = b ? "Sound on" : "Sound off";
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class LevelChanger : MonoBehaviour 
{
	public Text currentLevelText, infoText;
	public Button playButton;

	private void Start()
	{
		UpdateUI();
	}

	public void UpdateUI()
	{
		playButton.interactable = !GameManager.CurrentLevel.IsLocked();
		currentLevelText.text = GameManager.CurrentLevel.GetName();
		infoText.text = GameManager.CurrentLevel.GetInfo();
	}
	
}

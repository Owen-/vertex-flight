﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShopItem : MonoBehaviour 
{
	public Text titleText, buyText; //descriptionText?

	public string title; 
	public int price;
	public int max = 10;

	public ItemType itemType;

	private Shop shop;

	private const string buyFormatString = "({0}/{1})\n€ {2:n0}";

	public void UpdateUI()
	{
		int unlockedCount = SavedVariables.GetItemUnlockedCount(itemType);

		titleText.text = title;
		buyText.text = string.Format(buyFormatString, unlockedCount, max, price);
	}

	public void Initialize(Shop shop)
	{
		this.shop = shop;
		UpdateUI();
	}

	public void Reset()
	{
		SavedVariables.SetItemUnlockedCount(itemType, 0);
	}

	public void TryPurchase()
	{
		int credits = SavedVariables.GetCredits();
		int unlockedCount = SavedVariables.GetItemUnlockedCount(itemType);

		if(unlockedCount < max && credits >= price)
		{
			SavedVariables.SetItemUnlockedCount(itemType, unlockedCount+1 );
			SavedVariables.SetCredits( credits-price );
		}

		shop.UpdateUI();

		UpdateUI();
	}

}

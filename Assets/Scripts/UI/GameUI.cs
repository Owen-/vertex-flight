﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour 
{
	private Canvas canvas;
	private void Start()
	{
		GameManager.instance.OnEndGame += HideCanvas;
		GameManager.instance.OnStartGame += ShowCanvas;
		canvas = GetComponent<Canvas>();
	}

	private void OnDestroy()
	{
		GameManager.instance.OnEndGame -= HideCanvas;
		GameManager.instance.OnStartGame -= ShowCanvas;
	}

	public void ShowCanvas()
	{
		if (canvas!=null) canvas.enabled = true;
	}

	public void HideCanvas()
	{
		if (canvas!=null) canvas.enabled = false;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextureScroller : MonoBehaviour 
{
	public Vector2 scrollVector;
	private Material mat;
	
	protected void Start () 
	{
		mat = GetComponent<Renderer> ().material;
	}

	private void Update () 
	{
		if (mat != null)
			mat.mainTextureOffset = scrollVector*Time.time;
	}
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour 
{
	public static SoundManager instance = null;

	public AudioClip[] explosionSounds;
	public AudioClip[] shootSounds;
	public AudioClip[] damageSounds;
	public AudioClip[] pickupSounds;

	public AudioClip beatHighScore;
	public AudioClip menuSelect;
	public AudioClip bossDeath;
	public AudioClip bossSpawn;

	private void PlaySound(AudioClip clip)
	{
		if (clip != null)
			AudioSource.PlayClipAtPoint(clip, transform.position);
	}

	public void PlayExplosion(int i = -1)
	{
		if (i==-1) 
			i = Random.Range(0, explosionSounds.Length);
		
		PlaySound(explosionSounds[i]);
	}

	public void PlayShoot(int i = -1)
	{
		if (i==-1) 
			i = Random.Range(0, shootSounds.Length);
		
		PlaySound(shootSounds[i]);
	}

	public void PlayDamage(int i = -1)
	{
		if (i==-1) 
			i = Random.Range(0, damageSounds.Length);
		
		PlaySound(damageSounds[i]);
	}

	public void PlayPickup(int i = -1)
	{
		if (i==-1) 
			i = Random.Range(0, pickupSounds.Length);
		
		PlaySound(pickupSounds[i]);
	}

	public void PlayBeatHighScore()
	{
		PlaySound(beatHighScore);
	}

	public void PlayMenuSelect()
	{
		PlaySound(menuSelect);
	}

	public void PlayBossSpawn()
	{
		PlaySound(bossSpawn);
	}

	public void PlayBossDeath()
	{
		PlaySound(bossDeath);
	}

	private void Awake()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(gameObject);
	}

}

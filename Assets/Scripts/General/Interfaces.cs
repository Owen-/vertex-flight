﻿using UnityEngine;

public interface IKillable 
{
	int GetScorePoints();
	void Die();
}

public interface IDamageable
{
	void DoDamage(int damage, int power);
}
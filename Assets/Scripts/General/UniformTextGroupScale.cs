﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//class is designed for when you want to use "best fit" for multiple
//text elements, but need consistent font size
//currently this only works for static text

public class UniformTextGroupScale : MonoBehaviour 
{
	public Text[] textGroup;

	private IEnumerator Start()
	{
		yield return null;

		int minSize = textGroup[0].cachedTextGenerator.fontSizeUsedForBestFit;
		for (int i=0; i< textGroup.Length; i++)
		{
			if (textGroup[i].cachedTextGenerator.fontSizeUsedForBestFit < minSize)
			{
				minSize=textGroup[i].cachedTextGenerator.fontSizeUsedForBestFit;
			}
		}

		foreach (Text t in textGroup)
		{
			t.resizeTextMaxSize = minSize;
		}
	}
}